# MCKLAU003 - EEE3088F PiHat Project

Design for an Uninterrupted Power Supply (UPS) PiHat for a Raspberry Pi Zero.

The project requires that 3 submodules must be designed and simulated for the PiHat. These 3 submodules being a switching power regulator, 3 or more status LEDs and an amplifier circuit. The use cases of the project are to enable the use of the Raspberry Pi Zero during the length of load shedding, to allow the Raspberry Pi Zero to be portable while in use and as a safety mechanism against power cuts so that the Raspberry Pi is not damaged.

## How to Use the UPS PiHat

The PiHat connects to the Pi Zero through the use of the GPIO pins of the Pi Zero. The AC to DC converter of the UPS must be connected to the mains supply. This is so that the UPS can register that the mains supply has gone down and start supplying the Pi Zero with the use of the battery. The status LEDs will display the battery percentage remaining and is a self-regulating circuit. Each LED represents 10% of the full battery percentage. The batteries will need to be recharged separately. This UPS is very flexible as it can easily be adjusted to be used for different battery values. Once the UPS is connected to both the Pi Zero and the mains it will automatically switch between Mains Supply Mode and Battery Supply Mode when needed. No manual switching is required.

